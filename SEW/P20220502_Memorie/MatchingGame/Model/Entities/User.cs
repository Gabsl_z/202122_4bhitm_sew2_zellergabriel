﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entities;

[Table("USERS")]
public class User {

    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("ID")]
    public int Id { get; set; }

    [Column("NAME")]
    public string Name { get; set; }

    [Column("SCORE")]
    public int Score { get; set; }

    [Column("DATE")]
    public DateTime Date { get; set; }
}