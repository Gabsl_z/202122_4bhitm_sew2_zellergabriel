﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebGUI.Entities; 

public class MemoryCard {

    public MemoryCard(string content, int number) { 
        Content = content;
        Number = number;
    }
    
    public string Content { get; set; } = ":)";
    public int Number { get; set; }

    public bool IsOpen { get; set; }
}