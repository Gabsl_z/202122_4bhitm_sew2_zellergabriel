﻿using Microsoft.EntityFrameworkCore;
using Model.Entities;
using WebGUI.Entities;

namespace Model.Configurations; 

public class MemoryDbContext : DbContext {
    public DbSet<User> Users { get; set; }

    public MemoryDbContext(DbContextOptions options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder) { }
}