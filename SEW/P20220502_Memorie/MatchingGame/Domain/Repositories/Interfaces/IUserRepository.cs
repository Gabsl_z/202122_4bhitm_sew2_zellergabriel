﻿using Model.Entities;

namespace Domain.Repositories.Interfaces; 

public interface IUserRepository : IRepository<User> {
    Task<List<User>> ReadAllSortedAsync();
    Task<List<User>> ReadTodayAsync();
}