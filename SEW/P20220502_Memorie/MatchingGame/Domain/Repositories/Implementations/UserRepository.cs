﻿using Domain.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Model.Configurations;
using Model.Entities;

namespace Domain.Repositories.Implementations; 

public class UserRepository : ARepository<User>, IUserRepository {
    public UserRepository(MemoryDbContext dbContext) : base(dbContext) { }

    public async Task<List<User>> ReadAllSortedAsync() => await _table.OrderByDescending(u => u.Score).Take(10).ToListAsync();
    public async Task<List<User>> ReadTodayAsync() => await _table.OrderByDescending(u => u.Score).Where(u => u.Date == DateTime.Today).Take(10).ToListAsync();
}