﻿using System;
using System.Threading;


namespace P20210913_Thread_1 {
    internal class Program {
        public static void Main(string[] args) {
            ParameterizedThreadStart del;
            del = new ParameterizedThreadStart(Square);
            Thread thread = new Thread(del);
            thread.Start(3);
            
            //new Thread ((x)=>Console.WriteLine((int)x*(int)x)).Start(5);
        }
        static void Square(Object o) {
            int i = (int) o;
            Console.WriteLine(i*i);
        }
    }
}
