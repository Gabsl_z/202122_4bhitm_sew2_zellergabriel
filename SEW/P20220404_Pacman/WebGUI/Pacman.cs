﻿namespace WebGUI;

public class Pacman {
    public int X { get; private set; } = 100;
    public int Y { get; private set; } = 20;
    public int Step { get; set; } = 20;

    public void Move(EDirection which) {
        switch (which) {
            case EDirection.LEFT:
                X -= Step;
                break;
            case EDirection.RIGHT:
                X += Step;
                break;
            case EDirection.UP:
                Y -= Step;
                break;
            case EDirection.DOWN:
                Y += Step;
                break;
        }
    }
}