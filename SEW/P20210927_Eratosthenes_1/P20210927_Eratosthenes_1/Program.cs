﻿using System;
using System.Diagnostics;
using System.Threading;

namespace P20210927_Eratosthenes_1 {
    class Parameters {
        public bool[] prime;
    }

    class Program {
        static void Main(string[] args) {
            int n = 100;
            int pCount = 0;
            bool[] prime = new bool[n];
            
            for (int i = 2; i < n; i++)
            {
                prime[i] = true;
            }

            Parameters p = new Parameters {prime = prime};

            Thread a = new Thread(Sieb1);
            Thread b = new Thread(Sieb2);
            Thread s = new Thread(Sieb3);
            Thread t = new Thread(Sieb4);

            a.Start(p);
            b.Start(p);
            s.Start(p);
            t.Start(p);

            a.Join();
            b.Join();
            s.Join();
            t.Join();
            
            for (int i = 2; i < n; i++)
            {
                if (prime[i])
                    pCount++;
            }
            
            Console.WriteLine(pCount);
        }

        private static void Sieb1(object o)
        {
            bool[] prime = (o as Parameters).prime;
            int n = prime.Length;

          
            for (int i = 2; i * i < n; i++)
            {
               
                if (prime[i])
                {
                    for (int j = 2 * i; j <= n / 4; j += i)
                    {
                        //Dies kann unmöglich eine Primzahl sein
                        //weil es ein Vielfaches von i ist.
                        prime[j] = false;
                    }
                }
            }
        }
        private static void Sieb2(object o)
        {
            bool[] prime = (o as Parameters).prime;
            int n = prime.Length;

          
            for (int i = 2; i * i < n; i++)
            {
               
                if (prime[i])
                {
                    int start = n / 4 + 1;

                    if (start % i != 0)
                        start += (i - start % i);
                    
                    for (int j = start; j <= n / 2; j += i)
                    {
                        //Dies kann unmöglich eine Primzahl sein
                        //weil es ein Vielfaches von i ist.
                        prime[j] = false;
                    }
                }
            }
        }
        private static void Sieb3(object o)
        {
            bool[] prime = (o as Parameters).prime;
            int n = prime.Length;

          
            for (int i = 2; i * i < n; i++)
            {
               
                if (prime[i])
                {
                    int start = n / 2 + 1;

                    if (start % i != 0)
                        start += (i - start % i);
                    
                    for (int j = start; j <= (n / 4) * 3; j += i)
                    {
                        //Dies kann unmöglich eine Primzahl sein
                        //weil es ein Vielfaches von i ist.
                        prime[j] = false;
                    }
                }
            }
        }
        private static void Sieb4(object o)
        {
            bool[] prime = (o as Parameters).prime;
            int n = prime.Length;

            for (int i = 2; i * i < n; i++)
            {
                if (prime[i])
                {
                    int start = n / 4 * 3 + 1;

                    if (start % i != 0)
                        start += (i - start % i);

                    for (int j = start; j < n; j += i)
                    {
                        prime[j] = false;
                    }
                }
            }
        }
        /*private static void Sieb(object o) {
            bool[] prime = (o as Parameters).prime;
            int n = prime.Length;

            //Alle Produkte des Teilers i
            //angefangen bei 2, bis kleiner n durchlaufen
            //Wenn n = 50, dann ist bei i = 7 Schluss, weil das Produkt = 49 ist
            for (int i = 2; i * i < n; i++)
            {
                //Wenn die Zahl im Array als Primzahl markiert ist
                //Was bei den ersten beiden 2 und 3 definitiv der Fall ist
                if (prime[i])
                {
                    //Primzahl bis Wurzel(n) ausgeben
                    //Console.WriteLine(i);
                    //Alle weiteren Produkte des Teilers i
                    //angefangen beim Produkt i * i bis kleiner n durchlaufen
                    //j wird mit i beim nächsten Durchlauf (Vielfaches) addiert
                    for (int j = i * i; j < n; j += i)
                    {
                        //Dies kann unmöglich eine Primzahl sein
                        //weil es ein Vielfaches von i ist.
                        prime[j] = false;
                    }
                }
            }
        }*/
    }
}