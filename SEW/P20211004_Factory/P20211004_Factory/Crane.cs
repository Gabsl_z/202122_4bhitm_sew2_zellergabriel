﻿using System;
using System.Threading;

namespace P20211004_Factory {
    public class Crane {
        public static Semaphore cr = new Semaphore(0, 1);
        public static void Move(string from, string to) {
            Thread.Sleep(100);
            Console.WriteLine($"Crane: Moving workpiece from {from} to {to}");
        }
        
        public static void Run() {
            while (true) {
                
                Move("Storage1", "MachineA");
                MachineA.mA.Release();
                cr.WaitOne();
                Move("MachineA", "MachineB");
                MachineB.mB.Release();
                cr.WaitOne();
                Move("MachineB", "Storage2");
            }
        }
    }
}