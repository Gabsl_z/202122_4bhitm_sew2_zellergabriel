﻿using System;
using System.Threading;

namespace P20211004_Factory {
    public class MachineA {
        public static Semaphore mA = new Semaphore(0, 1);
        public static void Run() {
            while (true) {
                mA.WaitOne();
                Process();
                Crane.cr.Release();
            }
        }

        public static void Process() {
            Thread.Sleep(900);

            Console.WriteLine(
                "MachineA: finished process"
            );
        }
    }
}