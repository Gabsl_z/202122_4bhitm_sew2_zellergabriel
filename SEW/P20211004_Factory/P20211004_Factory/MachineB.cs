﻿using System;
using System.Threading;

namespace P20211004_Factory {
    public class MachineB {
        public static Semaphore mB = new Semaphore(0, 1);
        public static void Run() {
            while (true) {
                mB.WaitOne();
                Process();
                Crane.cr.Release();
            }
        }

        public static void Process() {
            Thread.Sleep(900);

            Console.WriteLine(
                "MachineB: finished process"
            );
        }
    }
}