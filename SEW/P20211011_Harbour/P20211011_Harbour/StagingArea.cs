﻿using System;
using System.Threading;

namespace P20211011_Harbour {
    public class StagingArea {
        public static Semaphore sH = new Semaphore(1, 4);

        public static void Run() {
            while (true)
            {
                sH.WaitOne();
                DockingArea.dH.Release();
            }
        }

        public static void Signal() {
            Thread.Sleep(1000);
            Console.WriteLine("StagingArea: signaling ship");
        }
    }
}