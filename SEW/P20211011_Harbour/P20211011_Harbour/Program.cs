﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace P20211011_Harbour {
    class Program {
        static void Main(string[] args) {
            List<Thread> threads = new List<Thread>();
            
            Thread harbour = new Thread(() => StagingArea.Run());

            for (int i = 0; i < 50; i++) {
                Ship s = new Ship();
                s.Id = i;
                threads.Add(new Thread(() => s.Run()));
            }
            
            harbour.Start();
            
            foreach (var t in threads) {
                t.Start();
            }
        }
    }
}