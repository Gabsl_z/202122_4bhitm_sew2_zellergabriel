﻿using System;
using System.Threading;

namespace P20211011_Harbour {
    public class Ship {
        public int Id { get; set; }
        public static Semaphore _shipSem = new Semaphore(4, 4);
        
        public void Run() {
            _shipSem.WaitOne();
            StagingArea.sH.WaitOne();
            DockingArea.dH.WaitOne();
            Unload();
            _shipSem.Release();
        }

        public void Unload() {
            Thread.Sleep(1000);
            Console.WriteLine($"unloading ship {Id}");
        }
    }
}