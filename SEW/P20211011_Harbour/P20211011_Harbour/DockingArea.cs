﻿using System;
using System.Threading;

namespace P20211011_Harbour {
    public class DockingArea {
        public static Semaphore dH = new Semaphore(0, 4);
        
        public static void Run() {
            while (true) {
                dH.WaitOne();
                Process();
                Ship._shipSem.Release();
            }
        }
        public static void Process() {
            Thread.Sleep(100);
            Console.WriteLine("processing ship");
        }
    }
}