﻿using System;
using System.Threading;

namespace P20211004_ConveyorBelt {
    public class MachineB {
        public static Semaphore mB = new Semaphore(0, 1);
        
        public static void Run() {
            while (true) {
                mB.WaitOne();
                mB.WaitOne();
                Process();
                ConveyorBelt.cB.Release();
                mB.Release();
            }
        }

        public static void Process() {
            Thread.Sleep(2000);
            Console.WriteLine(
                "MachineB: finished process"
            );
        }
    }
}