﻿using System;
using System.Threading;

namespace P20211004_ConveyorBelt {
    class Program {
        static void Main(string[] args) {
            
            Thread tA = new Thread(() => MachineA.Run());
            Thread tB = new Thread(() => MachineB.Run());
            Thread tC = new Thread(() => ConveyorBelt.Run());
            
            tA.Start();
            tB.Start();
            tC.Start();
        }
    }
}