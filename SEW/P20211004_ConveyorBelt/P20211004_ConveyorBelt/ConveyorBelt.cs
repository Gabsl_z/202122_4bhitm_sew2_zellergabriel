﻿using System;
using System.Threading;

namespace P20211004_ConveyorBelt {
    public class ConveyorBelt {
        public static Semaphore cB = new Semaphore(3, 3);

        public static void Move() {
            Thread.Sleep(1000);
            Console.WriteLine("ConveyorBelt moved");
        }
        
        public static void Run() {
            while (true) {
                
                cB.WaitOne();
                cB.WaitOne();
                Move();
                MachineA.mA.Release();
                MachineB.mB.Release();
            }
        }
    }
}