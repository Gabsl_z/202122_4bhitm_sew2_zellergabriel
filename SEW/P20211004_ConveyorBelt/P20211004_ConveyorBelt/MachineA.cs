﻿using System;
using System.Threading;

namespace P20211004_ConveyorBelt {
    public class MachineA {
        public static Semaphore mA = new Semaphore(0, 1);
         
        public static void Run() {
            while (true) {
                mA.WaitOne();
                Process();
                ConveyorBelt.cB.Release();
            }
        }

        public static void Process() {
            Thread.Sleep(2000);
            Console.WriteLine(
                "MachineA: finished process"
            );
        }
    }
}