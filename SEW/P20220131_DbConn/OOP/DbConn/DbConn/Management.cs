﻿using MySqlConnector;

namespace DbConn;

public class Management {
    MySqlConnection connection;
    private List<Student> StudentList;

    public Management() { }

    public Management(MySqlConnection connection) {
        this.connection = connection;
        connection.Open();
    }

    public void ReadAll() {
        using (var command = new MySqlCommand("SELECT * FROM students;", connection))
        using (var reader = command.ExecuteReader()) {
            while (reader.Read()) {
                Console.WriteLine($"{reader.GetValue(0)}: {reader.GetValue(1)} {reader.GetValue(2)}");
            }
        }
    }

    public void Read(int id) {
        string ret = "";
        using (var command = connection.CreateCommand()) {
            command.CommandText = "SELECT * FROM students WHERE STUDENT_ID = @prm_id;";
            command.Parameters.Add("@prm_id", MySqlDbType.Int32).Value = id;

            var erg = command.ExecuteReader();

            while (erg.Read()) {
                ret += $"{erg.GetValue(0)}: {erg.GetValue(1)} {erg.GetValue(2)}";
            }
        }

        Console.WriteLine(ret);
    }

    public void Insert(string fname, string lname, int schoolid) {
        string ret;
        using (var command = connection.CreateCommand()) {
            command.CommandText =
                $"INSERT INTO students (FIRST_NAME, LAST_NAME, SchoolId) VALUES ('{fname}', '{lname}', {schoolid})";

            var erg = command.ExecuteNonQuery();

            ret = erg.ToString();
        }

        Console.WriteLine(ret);
    }

    public void Delete(int id) {
        string ret;
        using (var command = connection.CreateCommand()) {
            command.CommandText = "DELETE FROM students WHERE STUDENT_ID = @prm_id;";
            command.Parameters.Add("@prm_id", MySqlDbType.Int32).Value = id;

            var erg = command.ExecuteNonQuery();

            ret = erg.ToString();
        }

        Console.WriteLine(ret);
    }

    public void Update(int id, string fname, string lname, int schoolid) {
        string ret;
        using (var command = connection.CreateCommand()) {
            command.CommandText =
                $"UPDATE students SET FIRST_NAME = '{fname}', LAST_NAME = '{lname}', SchoolId = {schoolid} WHERE STUDENT_ID = @prm_id";
            command.Parameters.Add("@prm_id", MySqlDbType.Int32).Value = id;

            var erg = command.ExecuteNonQuery();

            ret = erg.ToString();
        }

        Console.WriteLine(ret);
    }
}