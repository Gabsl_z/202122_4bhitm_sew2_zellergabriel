﻿using System.Data;
using MySqlConnector;
using Newtonsoft.Json;

namespace DbConn;

public class Management {
    MySqlConnection connection;
    private List<Student> StudentList;

    public Management() { }

    public Management(MySqlConnection connection) {
        this.connection = connection;
        if (this.connection.State != ConnectionState.Open) {
            connection.Open();
        }
    }

    public void ReadAll() {
        using (var command = new MySqlCommand("SELECT * FROM students;", connection))
        using (var reader = command.ExecuteReader()) {
            while (reader.Read()) {
                Console.WriteLine($"{reader.GetValue(0)}: {reader.GetValue(1)} {reader.GetValue(2)}");
            }
        }
    }

    public async Task<List<Student>> ReadAllAsync() {
        List<Student> studentList = new List<Student>();
        using (var command = new MySqlCommand("SELECT * FROM students;", connection))
        using (var reader = await command.ExecuteReaderAsync()) {
            while (reader.Read()) {
                studentList.Add(new Student {
                    Id = reader.GetInt32(0),
                    FirstName = reader.GetString(1),
                    LastName = reader.GetString(2),
                    SchoolId = reader.GetInt32(3)
                });
            }
        }
        return studentList;
    }
    

    public void Read(Student s) {
        string ret = "";
        using (var command = connection.CreateCommand()) {
            command.CommandText = "SELECT * FROM students WHERE STUDENT_ID = @prm_id;";
            command.Parameters.Add("@prm_id", MySqlDbType.Int32).Value = s.Id;

            var erg = command.ExecuteReader();

            while (erg.Read()) {
                ret += $"{erg.GetValue(0)}: {erg.GetValue(1)} {erg.GetValue(2)}";
            }
        }

        Console.WriteLine(ret);
    }

    public async Task<Student> ReadAsync(int id) {
        Student s = new Student();
        using (var command = connection.CreateCommand()) {
            command.CommandText = "SELECT * FROM students WHERE STUDENT_ID = @prm_id;";
            command.Parameters.Add("@prm_id", MySqlDbType.Int32).Value = id;

            using (var reader = await command.ExecuteReaderAsync()) {

                while (reader.Read()) {
                    s.Id = reader.GetInt32(0);
                    s.FirstName = reader.GetString(1);
                    s.LastName = reader.GetString(2);
                    s.SchoolId = reader.GetInt32(3);
                }
            }
        }
        return s;
    }
    

    public void Create(Student s) {
        string ret;
        using (var command = connection.CreateCommand()) {
            command.CommandText =
                $"INSERT INTO students (FIRST_NAME, LAST_NAME, SchoolId) VALUES ('{s.FirstName}', '{s.LastName}', {s.SchoolId})";

            var erg = command.ExecuteNonQuery();

            ret = erg.ToString();
        }

        Console.WriteLine(ret);
    }

    public async Task CreateAsync(Student s) {
        using (var command = connection.CreateCommand()) {
            command.CommandText = $"INSERT INTO students (FIRST_NAME, LAST_NAME, SchoolId) VALUES (@prm_fname, @prm_lname, @prm_sid)";
            command.Parameters.Add("@prm_fname", MySqlDbType.VarChar).Value = s.FirstName;
            command.Parameters.Add("@prm_lname", MySqlDbType.VarChar).Value = s.LastName;
            command.Parameters.Add("@prm_sid", MySqlDbType.Int32).Value = s.SchoolId;

            await command.ExecuteNonQueryAsync();
        }
    }

    public void Delete(Student s) {
        string ret;
        using (var command = connection.CreateCommand()) {
            command.CommandText = "DELETE FROM students WHERE STUDENT_ID = @prm_id;";
            command.Parameters.Add("@prm_id", MySqlDbType.Int32).Value = s.Id;

            var erg = command.ExecuteNonQuery();

            ret = erg.ToString();
        }

        Console.WriteLine(ret);
    }

    public async Task DeleteAsync(int id) {
        using (var command = connection.CreateCommand()) {
            command.CommandText = "DELETE FROM students WHERE STUDENT_ID = @prm_id;";
            command.Parameters.Add("@prm_id", MySqlDbType.Int32).Value = id;

            await command.ExecuteNonQueryAsync();
        }
    }

    public void Update(Student s) {
        string ret;
        using (var command = connection.CreateCommand()) {
            command.CommandText =
                $"UPDATE students SET FIRST_NAME = '{s.FirstName}', LAST_NAME = '{s.LastName}', SchoolId = {s.SchoolId} WHERE STUDENT_ID = @prm_id";
            command.Parameters.Add("@prm_id", MySqlDbType.Int32).Value = s.Id;

            var erg = command.ExecuteNonQuery();

            ret = erg.ToString();
        }

        Console.WriteLine(ret);
    }

    public async Task UpdateAsync(Student s) {
        using (var command = connection.CreateCommand()) {
            command.CommandText =
                $"UPDATE students SET FIRST_NAME = '{s.FirstName}', LAST_NAME = '{s.LastName}', SchoolId = {s.SchoolId} WHERE STUDENT_ID = @prm_id";
            command.Parameters.Add("@prm_id", MySqlDbType.Int32).Value = s.Id;

            await command.ExecuteNonQueryAsync();
        }
    }

    public string GetJSON() {
        using (var command = new MySqlCommand("SELECT * FROM students", connection)) {
            using (var reader = command.ExecuteReader()) {
                return SqlDataToJson(reader);
            }
        }
    }

    public string GetJSONById(int id) {
        using (var command = connection.CreateCommand()) {
            command.CommandText = "SELECT * FROM students WHERE STUDENT_ID = @prm_id;";
            command.Parameters.Add("@prm_id", DbType.Int32).Value = id;
            using (var reader = command.ExecuteReader()) {
                return SqlDataToJson(reader);
            }
        }
    }

    public string SqlDataToJson(MySqlDataReader dataReader) {
        var dataTable = new DataTable();
        dataTable.Load(dataReader);
        string JSONstring = string.Empty;
        JSONstring = JsonConvert.SerializeObject(dataTable);
        return JSONstring;
    }
}