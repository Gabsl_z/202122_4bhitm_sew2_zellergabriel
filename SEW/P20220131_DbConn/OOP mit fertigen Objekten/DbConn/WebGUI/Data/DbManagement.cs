﻿using DbConn;
using MySqlConnector;

namespace WebGUI.Data; 

public class DbManagement {

    public Management Management { get; }

    public static DbManagement Instance;

    private DbManagement() {
        MySqlConnection c = new("Server=localhost;User ID=htl;Password=insy;Database=school");
        Management = new Management(c);
    }

    public static DbManagement GetInstance() {
        if (Instance == null) {
            Instance = new DbManagement();
        }

        return Instance;
    }
}