﻿using DbConn;
using MySqlConnector;

MySqlConnection connection = new MySqlConnection("Server=localhost;User ID=htl;Password=insy;Database=school");
Management m = new(connection);

Student s1 = new Student {
    Id = 1
};
Student s2 = new Student {
    FirstName = "Carl",
    LastName = "Riedl",
    SchoolId = 1
};
Student s3 = new Student {
    Id = 11,
    FirstName = "Carl",
    LastName = "Porod",
    SchoolId = 1
};
Student s4 = new Student {
    Id = 8,
    FirstName = "Sieglinde",
    LastName = "Pichler",
    SchoolId = 5
};

Console.WriteLine("----------------");
Console.WriteLine("-- ReadAll");
Console.WriteLine("----------------");
//m.ReadAll();
Console.WriteLine("----------------");
Console.WriteLine("-- Read");
Console.WriteLine("----------------");
//m.Read(s1);
Console.WriteLine("----------------");
Console.WriteLine("-- Insert");
Console.WriteLine("----------------");
//m.Insert(s3);
Console.WriteLine("----------------");
Console.WriteLine("-- Delete");
Console.WriteLine("----------------");
//m.Delete(s3);
Console.WriteLine("----------------");
Console.WriteLine("-- Update");
Console.WriteLine("----------------");
//m.Update(s4);

Console.WriteLine("----------------");
Console.WriteLine("-- GetJSON");
Console.WriteLine("----------------");
Console.WriteLine(m.GetJSON());
Console.WriteLine("----------------");
Console.WriteLine("-- GetJSONById");
Console.WriteLine("----------------");
Console.WriteLine(m.GetJSONById(1));