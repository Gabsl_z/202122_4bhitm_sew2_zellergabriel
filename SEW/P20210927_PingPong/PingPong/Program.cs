﻿using System;
using System.Threading;

namespace Sieb_des_Eratosthenes {
    public class Sems {
        public static Semaphore _ping, _pong;

        static Sems() {
            _ping = new Semaphore(0, 1);
            _pong = new Semaphore(0, 1);
        }
    }

    public class Program {
        static void Main(string[] args) {

            Example ex = new Example();

            Thread p = new Thread(new ThreadStart(ex.Ping));
            Thread c = new Thread(new ThreadStart(ex.Pong));
            Sems._pong.Release();
            
            p.Start();
            c.Start();
        }
    }

    public class Example {
        int i = 20;

        public void Ping() {
            while (i > 0)
            {
                Sems._ping.WaitOne();
                Console.WriteLine("Ping = {0,5} Thread = {1,3}", i, Thread.CurrentThread.GetHashCode().ToString());

                i--;
                Sems._pong.Release();
            }
        }

        public void Pong() {
            while (i > 0)
            {
                Sems._pong.WaitOne();
                Console.WriteLine("Pong = {0,5} Thread = {1,3}", i, Thread.CurrentThread.GetHashCode().ToString());
                i--;
                Sems._ping.Release();
            }
        }
    }
}