﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.Configuration;

namespace KebabAPI.Repositories; 

public abstract class ARepository<TEntity> : IRepository<TEntity> where TEntity : class{
    
    private readonly KebabDbContext _context;
    private readonly DbSet<TEntity> _entities;

    protected ARepository(KebabDbContext context){
        _context = context;
        _entities = context.Set<TEntity>();
    }
    public async Task<TEntity> ReadAsync(int id) => await _entities.FindAsync(id);

    public async Task<List<TEntity>> ReadAllAsync() => await _entities.ToListAsync();

    public async Task CreateAsync(TEntity entity){
        _entities.Add(entity);
        await _context.SaveChangesAsync();
    }
    public async Task DeleteAsync(TEntity entity){
        _entities.Remove(entity);
        await _context.SaveChangesAsync();
    }
    public async Task UpdateAsync(TEntity entity){
        _context.ChangeTracker.Clear();
        _entities.Update(entity);
        await _context.SaveChangesAsync();
    }
}