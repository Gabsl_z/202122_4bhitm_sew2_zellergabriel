﻿using Model;
using Model.Configuration;
using Model.Entity;

namespace KebabAPI.Repositories; 

public class IngredientRepository : ARepository<Ingredient> {
    public IngredientRepository(KebabDbContext context) : base(context) {
    }
}