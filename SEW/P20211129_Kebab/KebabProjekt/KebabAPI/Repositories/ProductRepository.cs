﻿using Model.Configuration;
using Model.Entity;

namespace KebabAPI.Repositories; 

public class ProductRepository : ARepository<Product> {
    public ProductRepository(KebabDbContext context) : base(context) {
    }
}