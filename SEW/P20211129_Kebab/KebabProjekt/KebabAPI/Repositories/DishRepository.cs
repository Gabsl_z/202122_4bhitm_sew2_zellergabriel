﻿using Model;
using Model.Configuration;
using Model.Entity;

namespace KebabAPI.Repositories; 

public class DishRepository : ARepository<Dish> {
    public DishRepository(KebabDbContext context) : base(context) {
    }
}