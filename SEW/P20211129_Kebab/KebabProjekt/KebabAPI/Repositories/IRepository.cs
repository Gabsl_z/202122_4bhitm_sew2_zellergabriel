﻿namespace KebabAPI.Repositories; 

public interface IRepository<TEntity> where TEntity : class {
    Task<TEntity> ReadAsync(int id);
    Task<List<TEntity>> ReadAllAsync();

    Task CreateAsync(TEntity entity);

    Task UpdateAsync(TEntity entity);

    Task DeleteAsync(TEntity entity);
}