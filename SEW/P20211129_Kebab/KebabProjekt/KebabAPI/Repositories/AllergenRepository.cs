﻿using Model.Configuration;
using Model.Entity;

namespace KebabAPI.Repositories; 

public class AllergenRepository : ARepository<Allergene> {
    public AllergenRepository(KebabDbContext context) : base(context) {
    }
}