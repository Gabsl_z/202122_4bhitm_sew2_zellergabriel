﻿using Model;
using Model.Configuration;
using Model.Entity;

namespace KebabAPI.Repositories; 

public class DrinkRepository : ARepository<Drink> {
    public DrinkRepository(KebabDbContext context) : base(context) {
    }
}