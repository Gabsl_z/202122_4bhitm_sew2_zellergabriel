﻿using KebabAPI.Repositories;
using Model;
using Model.Entity;

namespace KebabAPI.Controllers; 

public class DrinkController : AController<Drink> {
    public DrinkController(IRepository<Drink> repository) : base(repository) {
    }
}