using KebabAPI.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace KebabAPI.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AController<TEntity>: ControllerBase where TEntity : class{

    private IRepository<TEntity> _repository;

    public AController(IRepository<TEntity> repository){
        _repository = repository;
    }

    //Read, ReadAll, Update, Delete, Create

    [HttpGet("read/{id}")]
    public async Task<ActionResult<TEntity>> ReadAsync([Required] int id) => Ok(await _repository.ReadAsync(id));

    [HttpGet("readAll")]
    public async Task<ActionResult<List<TEntity>>> ReadAsync() => Ok(await _repository.ReadAllAsync());

    [HttpPost("create")]
    public async Task<ActionResult<TEntity>> CreateAsync(TEntity entity){
        await _repository.CreateAsync(entity);
        return Ok(entity);
    }
    
    [HttpPut("update/{id}")]
    public async Task<ActionResult> UpdateAsync([Required]int id,[Required] TEntity entity){
        var c = await _repository.ReadAsync(id);
        if (c is null) {
            return NotFound();
        }

        await _repository.UpdateAsync(entity);
        return NoContent();
    }

    [HttpDelete("delete/{id}")]
    public async Task<ActionResult> DeleteAsync([Required] int id){
        var e = await _repository.ReadAsync(id);

        if (e is null) {
            return NotFound();
        }
        
        await _repository.DeleteAsync(e);
        return NoContent();
    }
}