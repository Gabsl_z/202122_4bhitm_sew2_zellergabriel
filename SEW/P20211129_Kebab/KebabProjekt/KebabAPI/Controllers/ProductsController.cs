using KebabAPI.Repositories;
using Model.Entity;

namespace KebabAPI.Controllers;

    public class ProductsController : AController<Product> {
        public ProductsController(IRepository<Product> repository) : base(repository) {
        }
    }