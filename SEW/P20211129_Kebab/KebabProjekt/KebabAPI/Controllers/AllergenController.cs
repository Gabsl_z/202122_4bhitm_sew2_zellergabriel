﻿using KebabAPI.Repositories;
using Microsoft.AspNetCore.Mvc;
using Model;
using Model.Entity;

namespace KebabAPI.Controllers; 

public class AllergenController : AController<Allergene> {
    public AllergenController(IRepository<Allergene> repository) : base(repository) {
    }
}