﻿using KebabAPI.Repositories;
using Model.Entity;

namespace KebabAPI.Controllers; 

public class DishController : AController<Dish> {
    public DishController(IRepository<Dish> repository) : base(repository) {
    }
}