﻿using KebabAPI.Repositories;
using Model;
using Model.Entity;

namespace KebabAPI.Controllers; 

public class IngredientController : AController<Ingredient> {
    public IngredientController(IRepository<Ingredient> repository) : base(repository) {
    }
}