﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Model.Entity;

namespace Model.Configuration; 

public class KebabDbContext : DbContext{
    public KebabDbContext(DbContextOptions<KebabDbContext> options) : base(options) {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Drink> Drinks { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Allergene> Allergenes { get; set; }
        public DbSet<OrderHasProductsJT> OrderHasProductsJts { get; set; }
        public DbSet<DishHasAllergensJT> DishHasAllergensJts { get; set; }
        public DbSet<DishHasIngredientsJT> DishHasIngredientsJts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder) {
            builder.Entity<OrderHasProductsJT>().HasOne(o => o.Product).WithMany().HasForeignKey(o => o.ProductId);
            builder.Entity<OrderHasProductsJT>().HasOne(o => o.Order).WithMany().HasForeignKey(o => o.OrderID);
            builder.Entity<OrderHasProductsJT>().HasKey(o => new {o.ProductId, o.OrderID});
            builder.Entity<DishHasAllergensJT>().HasOne(o => o.Allergene).WithMany().HasForeignKey(o => o.Code);
            builder.Entity<DishHasAllergensJT>().HasOne(o => o.Dish).WithMany().HasForeignKey(o => o.DishId);
            builder.Entity<DishHasAllergensJT>().HasKey(o => new {o.Code, o.DishId});
            builder.Entity<DishHasIngredientsJT>().HasOne(o => o.Ingredient).WithMany().HasForeignKey(o => o.IngredientValue);
            builder.Entity<DishHasIngredientsJT>().HasOne(o => o.Dish).WithMany().HasForeignKey(o => o.DishId);
            builder.Entity<DishHasIngredientsJT>().HasKey(o => new {o.IngredientValue, o.DishId});
                builder.Entity<Ingredient>().Property(ingredient => ingredient.Value).HasConversion(new EnumToStringConverter<EIngredient>());
            builder.Entity<DishHasIngredientsJT>().Property(ingredient => ingredient.IngredientValue).HasConversion(new EnumToStringConverter<EIngredient>());
            builder.Entity<Allergene>().Property(allergene => allergene.Code).HasConversion(new EnumToStringConverter<EAllergene>());
            builder.Entity<DishHasAllergensJT>().Property(allergene => allergene.Code).HasConversion(new EnumToStringConverter<EAllergene>());
            builder.Entity<Product>().Property(product => product.Category).HasConversion(new EnumToStringConverter<ECategory>());
        }
}