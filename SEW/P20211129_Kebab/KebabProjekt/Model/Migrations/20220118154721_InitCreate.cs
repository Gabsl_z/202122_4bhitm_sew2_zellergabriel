﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Model.Migrations
{
    public partial class InitCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "ALLERGENS",
                columns: table => new
                {
                    CODE = table.Column<string>(type: "VARCHAR(1)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    DESCRIPTION = table.Column<string>(type: "TEXT", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ALLERGENS", x => x.CODE);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "E_INGREDIENTS",
                columns: table => new
                {
                    VALUE = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_E_INGREDIENTS", x => x.VALUE);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "ORDERS",
                columns: table => new
                {
                    ORDER_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ORDER_DATE = table.Column<DateTime>(type: "TIMESTAMP", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ORDERS", x => x.ORDER_ID);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "PRODUCTS_BT",
                columns: table => new
                {
                    PRODUCT_ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    NAME = table.Column<string>(type: "VARCHAR(45)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PRICE = table.Column<decimal>(type: "DECIMAL(6,2)", nullable: false),
                    CATEGORY = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PRODUCTS_BT", x => x.PRODUCT_ID);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "DISHES",
                columns: table => new
                {
                    PRODUCT_ID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DISHES", x => x.PRODUCT_ID);
                    table.ForeignKey(
                        name: "FK_DISHES_PRODUCTS_BT_PRODUCT_ID",
                        column: x => x.PRODUCT_ID,
                        principalTable: "PRODUCTS_BT",
                        principalColumn: "PRODUCT_ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "DRINKS",
                columns: table => new
                {
                    PRODUCT_ID = table.Column<int>(type: "int", nullable: false),
                    SIZE = table.Column<decimal>(type: "DECIMAL(3,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DRINKS", x => x.PRODUCT_ID);
                    table.ForeignKey(
                        name: "FK_DRINKS_PRODUCTS_BT_PRODUCT_ID",
                        column: x => x.PRODUCT_ID,
                        principalTable: "PRODUCTS_BT",
                        principalColumn: "PRODUCT_ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "ORDER_HAS_PRODUCTS_JT",
                columns: table => new
                {
                    ORDER_ID = table.Column<int>(type: "int", nullable: false),
                    PRODUCT_ID = table.Column<int>(type: "int", nullable: false),
                    DISCOUNT = table.Column<decimal>(type: "DECIMAL(3,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ORDER_HAS_PRODUCTS_JT", x => new { x.PRODUCT_ID, x.ORDER_ID });
                    table.ForeignKey(
                        name: "FK_ORDER_HAS_PRODUCTS_JT_ORDERS_ORDER_ID",
                        column: x => x.ORDER_ID,
                        principalTable: "ORDERS",
                        principalColumn: "ORDER_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ORDER_HAS_PRODUCTS_JT_PRODUCTS_BT_PRODUCT_ID",
                        column: x => x.PRODUCT_ID,
                        principalTable: "PRODUCTS_BT",
                        principalColumn: "PRODUCT_ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "DISH_HAS_ALLERGENS_JT",
                columns: table => new
                {
                    DISH_ID = table.Column<int>(type: "int", nullable: false),
                    ALLERGENES_CODE = table.Column<string>(type: "VARCHAR(1)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DISH_HAS_ALLERGENS_JT", x => new { x.ALLERGENES_CODE, x.DISH_ID });
                    table.ForeignKey(
                        name: "FK_DISH_HAS_ALLERGENS_JT_ALLERGENS_ALLERGENES_CODE",
                        column: x => x.ALLERGENES_CODE,
                        principalTable: "ALLERGENS",
                        principalColumn: "CODE",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DISH_HAS_ALLERGENS_JT_DISHES_DISH_ID",
                        column: x => x.DISH_ID,
                        principalTable: "DISHES",
                        principalColumn: "PRODUCT_ID",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "DISH_HAS_INGREDIENTS_JT",
                columns: table => new
                {
                    DISH_ID = table.Column<int>(type: "int", nullable: false),
                    INGREDIENT_VALUE = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DISH_HAS_INGREDIENTS_JT", x => new { x.INGREDIENT_VALUE, x.DISH_ID });
                    table.ForeignKey(
                        name: "FK_DISH_HAS_INGREDIENTS_JT_DISHES_DISH_ID",
                        column: x => x.DISH_ID,
                        principalTable: "DISHES",
                        principalColumn: "PRODUCT_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DISH_HAS_INGREDIENTS_JT_E_INGREDIENTS_INGREDIENT_VALUE",
                        column: x => x.INGREDIENT_VALUE,
                        principalTable: "E_INGREDIENTS",
                        principalColumn: "VALUE",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_DISH_HAS_ALLERGENS_JT_DISH_ID",
                table: "DISH_HAS_ALLERGENS_JT",
                column: "DISH_ID");

            migrationBuilder.CreateIndex(
                name: "IX_DISH_HAS_INGREDIENTS_JT_DISH_ID",
                table: "DISH_HAS_INGREDIENTS_JT",
                column: "DISH_ID");

            migrationBuilder.CreateIndex(
                name: "IX_ORDER_HAS_PRODUCTS_JT_ORDER_ID",
                table: "ORDER_HAS_PRODUCTS_JT",
                column: "ORDER_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DISH_HAS_ALLERGENS_JT");

            migrationBuilder.DropTable(
                name: "DISH_HAS_INGREDIENTS_JT");

            migrationBuilder.DropTable(
                name: "DRINKS");

            migrationBuilder.DropTable(
                name: "ORDER_HAS_PRODUCTS_JT");

            migrationBuilder.DropTable(
                name: "ALLERGENS");

            migrationBuilder.DropTable(
                name: "DISHES");

            migrationBuilder.DropTable(
                name: "E_INGREDIENTS");

            migrationBuilder.DropTable(
                name: "ORDERS");

            migrationBuilder.DropTable(
                name: "PRODUCTS_BT");
        }
    }
}
