﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity {
    [Table("DISH_HAS_ALLERGENS_JT")]
    public class DishHasAllergensJT {

        [Column("DISH_ID")]
        public int DishId { get; set; }

        public Dish Dish { get; set; }

        [Column("ALLERGENES_CODE", TypeName = "VARCHAR(1)")]
        public EAllergene Code { get; set; }

        public Allergene Allergene { get; set; }
    }
}