﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity {
    [Table("DISH_HAS_INGREDIENTS_JT")]
    public class DishHasIngredientsJT {
        [Column("DISH_ID")]
        public int DishId { get; set; }

        public Dish Dish { get; set; }

        [Column("INGREDIENT_VALUE")]
        public EIngredient IngredientValue { get; set; }

        public Ingredient Ingredient { get; set; }
    }
}