﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity {
    
    [Table("PRODUCTS_BT")]
    public class Product {
        
        [Column("PRODUCT_ID")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("NAME", TypeName = "VARCHAR(45)")]
        [Required]
        public string Name { get; set; }

        [Column("PRICE", TypeName = "DECIMAL(6,2)")]
        [Required]
        public float Price { get; set; }
        
        [Column("CATEGORY")]
        [Required]
        public ECategory Category { get; set; }
    }
}