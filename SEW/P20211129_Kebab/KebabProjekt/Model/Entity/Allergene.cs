﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity {
    [Table("ALLERGENS")]
    public class Allergene {

        [Column("CODE", TypeName = "VARCHAR(1)")]
        [Key]
        public EAllergene Code { get; set; }

        [Column("DESCRIPTION", TypeName = "TEXT")]
        [Required]
        public string Description { get; set; }
    }
}