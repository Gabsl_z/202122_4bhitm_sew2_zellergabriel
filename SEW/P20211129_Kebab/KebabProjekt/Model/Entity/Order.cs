﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity {
    
    [Table("ORDERS")]
    public class Order {
        
        [Column("ORDER_ID")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("ORDER_DATE", TypeName = "TIMESTAMP")]
        [Required]
        public DateTime Date { get; set; }
    }
}