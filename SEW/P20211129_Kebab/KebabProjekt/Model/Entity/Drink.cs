﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity {
    
    [Table("DRINKS")]
    public class Drink : Product {

        [Column("SIZE", TypeName = "DECIMAL(3,2)")]
        [Required]
        public float Size { get; set; }
    }
}