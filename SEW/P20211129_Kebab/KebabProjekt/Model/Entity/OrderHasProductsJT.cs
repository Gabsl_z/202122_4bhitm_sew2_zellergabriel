﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity {
    [Table("ORDER_HAS_PRODUCTS_JT")]
    public class OrderHasProductsJT {

        [Column("ORDER_ID")]
        public int OrderID { get; set; }

        public Order Order { get; set; }
        
        [Column("PRODUCT_ID")]
        public int ProductId { get; set; }

        public Product Product { get; set; }

        [Column("DISCOUNT", TypeName = "DECIMAL(3,2)")]
        [Required]
        public float Discount { get; set; }
    }
}