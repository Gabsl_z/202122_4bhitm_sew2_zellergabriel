﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity {
    
    [Table("E_INGREDIENTS")]
    public class Ingredient {
        
        [Column("VALUE")]
        [Key]
        public EIngredient Value { get; set; }
    }
}