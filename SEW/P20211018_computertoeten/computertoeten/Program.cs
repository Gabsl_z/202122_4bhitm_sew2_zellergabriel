﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace computertoeten {
    class Program {
        private static int cnt;
        //private static Semaphore s = new Semaphore(0, 0);
        static void Main(string[] args) {
            const int ANZ = 500;

            Stopwatch sw = new Stopwatch();
            Task[] tasks = new Task[ANZ];
            
            sw.Start();
            for (int i = 0; i < ANZ; i++)
            {
                tasks[i] = new Task(Tuwas);
                tasks[i].Start();
            }

            for (int i = 0; i < ANZ; i++)
            {
                tasks[i].Wait();
            }
            sw.Stop();
            Console.WriteLine(sw.Elapsed);
            Console.WriteLine(cnt);
        }

        private static void Tuwas() {
            cnt++;
            string x = "A" + "B";
        }
    }
}