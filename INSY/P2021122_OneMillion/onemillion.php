<?php
include_once "includes/dbh.php";
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <title>Document</title>
</head>
<body>
<form action="includes/addData.php" method="post">
    <button class="btn btn-success m-5" type="submit"><i class="bi bi-check-square"></i> Add Data</button>
</form>
<form action="includes/selectData.php" method="post">
    <button class="btn btn-warning mx-5" type="submit"><i class="bi bi-check-square"></i> Select Data</button>
</form>

<?php
if (isset($_GET["time"])){
    echo "<h3 class='mx-5'>{$_GET["time"]}</h3>";
}
?>
</body>
</html>
