<?php
include_once "dbh.php";

$time_pre = microtime(true);

for ($i = 0; $i < 100; $i++) {
    $sql = "SELECT count(id) FROM Numbers WHERE id between 1 and 1000000";
    mysqli_query($conn, $sql);
}

$time_post = microtime(true);
$exec_time = $time_post - $time_pre;

header("Location: ../onemillion.php?time=$exec_time");